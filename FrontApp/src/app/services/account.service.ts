import { Injectable } from '@angular/core';

import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Account } from '../models/account.model';
import { FormGroup, FormControl } from '@angular/forms';
import * as firebase from 'firebase/app';
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireObject, AngularFireList } from 'angularfire2/database';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class AccountService {
  dbPath = '/accounts';
  accountRef: AngularFireList<Account> = null;

  constructor(public db: AngularFireDatabase) {
    this.accountRef = db.list(this.dbPath);
  }

  createAccount(account: Account): void {
    this.accountRef.push(account);
  }

  getAccount(key: string): AngularFireObject<Account> {
    return this.db.object('accounts/' + key);
  }

  updateAccount(key: string, value: any): void {
    this.accountRef.update(key, value).catch(error => this.handleError(error));
  }

  deleteAccount(key: string): void {
    this.accountRef.remove(key).catch(error => this.handleError(error));
  }

  getAccountsList() {
    //return this.db.list('accounts/')
    /*return this.accountRef.snapshotChanges().map(accounts => {
      return accounts.map(action => ({ key: action.key, ...action.payload.val() }));
    }).subscribe(items => {
      return items.map(item => item.key);
    });*/



    /* .map(accounts => {
      return accounts.map(a => {
        data = a.payload.val();
        key = a.payload.key;
      });
    }); */

    return this.accountRef;
  }

  deleteAll(): void {
    this.accountRef.remove().catch(error => this.handleError(error));
  }

  private handleError(error) {
    console.log(error);
  }
}