import { Injectable } from '@angular/core';
import { AngularFireList, AngularFireDatabase } from 'angularfire2/database';
import { Transaction } from '../models/transaction.model';

@Injectable({
  providedIn: 'root'
})
export class TransactionService {
  dbPath = '/transactions';
  transactionRef: AngularFireList<Transaction> = null;

  constructor(db: AngularFireDatabase) {
    this.transactionRef = db.list(this.dbPath);
  }

  createTransaction(transaction: Transaction): void {
    this.transactionRef.push(transaction);
  }

  updateTransaction(key: string, value: any): void {
    this.transactionRef.update(key, value).catch(error => this.handleError(error));
  }

  deleteTransaction(key: string): void {
    this.transactionRef.remove(key).catch(error => this.handleError(error));
  }

  getTransactionsList(): AngularFireList<Transaction> {
    return this.transactionRef;
  }

  deleteAll(): void {
    this.transactionRef.remove().catch(error => this.handleError(error));
  }

  private handleError(error) {
    console.log(error);
  }
}
