import { MatNativeDateModule, MatInputModule, MatButtonModule, MatCardModule, MatFormFieldModule, MatCheckboxModule, MatDatepickerModule, MatRadioModule, MatSelectModule } from '@angular/material';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { environment } from '../environments/environment';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './components/app/app.component';
import { AccountsComponent } from './components/accounts/accounts.component';
import { MatIconModule } from '@angular/material/icon';
import { MatDialogModule } from '@angular/material/dialog';

import { AccountService } from './services/account.service';
import { HttpClientModule } from '@angular/common/http';
import { AngularFirestore } from 'angularfire2/firestore';
import { HomeComponent } from './pages/home/home.component';
import { CustomEuroPipe } from './pipes/custom-euro.pipe';
import { AccountComponent } from './pages/account/account.component';
import { TransactionComponent } from './components/transaction/transaction.component';
import { TransactionService } from './services/transaction.service';
import { AddTransactionComponent } from './components/transaction/add-transaction/add-transaction.component';
import { AddTransactionDialogComponent } from './components/transaction/add-transaction/add-transaction-dialog/add-transaction-dialog.component';
@NgModule({
  declarations: [
    AppComponent,
    AccountsComponent,
    HomeComponent,
    CustomEuroPipe,
    AccountComponent,
    TransactionComponent,
    AddTransactionComponent,
    AddTransactionDialogComponent
  ],
  entryComponents: [
    AddTransactionDialogComponent,
  ],
  exports: [
    MatDatepickerModule
  ],
  imports: [
    BrowserModule,
    MatSelectModule,
    MatRadioModule,
    MatCardModule,
    AppRoutingModule,
    FormsModule,
    BrowserAnimationsModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireDatabaseModule,
    MatButtonModule,
    HttpClientModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatIconModule,
    MatInputModule,
    MatFormFieldModule,
    MatDialogModule,
    MatNativeDateModule,
  ],
  providers: [
    AccountService,
    TransactionService,
    AngularFirestore,
    MatDatepickerModule,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
