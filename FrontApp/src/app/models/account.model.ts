import { Transaction } from './transaction.model';

export class Account {
    key?: string;
    name: string;
    solde: number;
    owner: string;
    transaction?: Transaction[];
}
