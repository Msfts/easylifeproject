export class Transaction {

    id?: string;
    name?: string;
    solde?: number;
    isDepense: boolean;
    dateTransaction: Date;
}
