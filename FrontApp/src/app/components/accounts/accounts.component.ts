import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AccountService } from '../../services/account.service';
import { Account } from '../../models/account.model';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import 'rxjs/Rx';

@Component({
  selector: 'app-accounts',
  templateUrl: './accounts.component.html',
  styleUrls: ['./accounts.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AccountsComponent implements OnInit {
  accounts: Account[] = [];
  temp: Promise<{}[]>;

  public account: Account;
  constructor(private accountService: AccountService, ) {
    this.account = {
      name: "",
      owner: "",
      solde: 0,
    };
  }

  ngOnInit() {
    this.accountService.getAccountsList().snapshotChanges().pipe(
      map(changes =>
        changes.map(c => ({ key: c.payload.key, ...c.payload.val() }))
      )
    ).subscribe(accounts => {
      accounts.forEach(element => {
        this.accounts.push(element);
      });
    });
  }
}