import { Component, OnInit, ViewEncapsulation, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Transaction } from '../../../../models/transaction.model';
import { AccountService } from 'src/app/services/account.service';
import { TransactionService } from 'src/app/services/transaction.service';
import { AngularFireList } from 'angularfire2/database';
import { map } from 'rxjs/operators';


export interface dataTransaction {
  transaction: Transaction;
  accountKey: string;
}

@Component({
  selector: 'app-add-transaction-dialog',
  templateUrl: './add-transaction-dialog.component.html',
  styleUrls: ['./add-transaction-dialog.component.scss'],
  encapsulation: ViewEncapsulation.None,

})
export class AddTransactionDialogComponent implements OnInit {
  public accountKey: string;
  public transaction: Transaction = this.data.transaction;
  public accounts: any;
  public account: any;
  constructor(
    public dialogRef: MatDialogRef<AddTransactionDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: dataTransaction,
    public accountService: AccountService,
    public transactionService: TransactionService,

  ) {
    this.transaction = this.data.transaction;
    this.accountKey = this.data.accountKey;
  }

  ngOnInit() {
  }

  addransaction() {
    this.transactionService.createTransaction(this.transaction);
    this.accounts = this.accountService.getAccountsList();

    this.accountService.getAccount(this.accountKey).snapshotChanges().pipe(
      map(changes =>
        changes.map(c => ({ key: c.payload.key, ...c.payload.val() }))
      )
    ).subscribe(accounts => {
      accounts.forEach(element => {
        this.accounts.push(element);
      });
    });


    this.accountService.getAccount(this.accountKey).snapshotChanges().map(changes => {
      changes.payload.key
    }).subscribe(account => {
      this.account = account;
      console.log("TCL: AccountsComponent -> ngOnInit -> this.accounts", this.accounts)
    });
    //this.accounts
    //this.accountService.updateAccount(this.accountKey, )
  }

}
