import { Component, OnInit, ViewEncapsulation, Input } from '@angular/core';
import { Transaction } from 'src/app/models/transaction.model';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AddTransactionDialogComponent } from './add-transaction-dialog/add-transaction-dialog.component';

@Component({
  selector: 'app-add-transaction',
  templateUrl: './add-transaction.component.html',
  styleUrls: ['./add-transaction.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AddTransactionComponent implements OnInit {
  public transaction: Transaction;

  @Input() accountKey: string;

  constructor(public dialog: MatDialog) {
    this.transaction = {
      solde: 0,
      isDepense: false,
      dateTransaction: new Date(),
    };
  }

  openDialog(accountKey: string): void {
    const dialogRef = this.dialog.open(AddTransactionDialogComponent, {
      width: '40vw',
      height: '40vh',
      data: { transaction: this.transaction, accountKey: accountKey },
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  ngOnInit() {
  }

  addTransaction(accountKey: string) {
    this.transaction.isDepense = false;
    this.openDialog(accountKey);
  }

  removeTransaction(accountKey: string) {
    this.transaction.isDepense = true;
    this.openDialog(accountKey);
  }


}
