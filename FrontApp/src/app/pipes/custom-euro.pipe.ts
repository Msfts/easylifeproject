import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'customEuro'
})
export class CustomEuroPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    return value + '€';
  }

}
